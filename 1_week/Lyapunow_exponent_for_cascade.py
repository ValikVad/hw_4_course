from typing import List
import numpy as np
import matplotlib.pyplot as plt


def function_Vadim(point: float, param: List[float]) -> float:
    ret_point = param[0] * np.sin(point) * (1 - np.sin(point))
    return ret_point


def function_Tat(point: float, param: List[float]) -> float:
    ret_point = param[0] * point * (1 - point ** 2)
    return ret_point


def calculate_Lyapunov_exponent_in_point(func, point: float, params: List, eps: float, M: int, T:int) -> float:
    sum = 0.
    point_eps = point + eps

    for i in range(T):
        for j in range(M):
            point = func(point, params)
            point_eps = func(point_eps, params)
            dist = abs(point - point_eps)
            point_eps = point + eps
        sum += np.log(dist / eps)
    result = sum / (T * M)
    return result
kek = []
b = np.linspace(0, 6, 10000)
for index in range(10000):
    print(index)
    params = [b[index]]
    kek.append(calculate_Lyapunov_exponent_in_point(function_Vadim, 0.1, params, 0.0001, 100, 100))

# print(kek, b)
plt.plot([0, 6], [0, 0], c='black')
plt.plot(b, kek, c='black')
plt.show()
