from typing import List
import numpy as np
import matplotlib.pyplot as plt


def function_vadim(input_point: float, input_param: List[float]) -> float:
    ret_point = input_param[0] * np.sin(input_point) * (1 - np.sin(input_point))
    return ret_point


def function_tat(input_point: float, input_param: List[float]) -> float:
    ret_point = input_param[0] * input_point * (1 - input_point ** 2)
    return ret_point


point = 0.3  # start point
param = [3]  # params for our function
N = 100  # number of steps for diagram of Lam

x = np.linspace(-8, 5, 10000)  # initialize np.ndarray x coord

#  initialize lists of horizontal and vertical coordinates of line our diagram
x_lam = []
y_lam = []
x_lam1 = []
y_lam1 = []
point1 = point  # rename only for me

#  complete coordinates
for i in range(N):
    point2 = function_vadim(point1, param)
    for k in range(200):
        x_lam.append(point1)
        y_lam1.append(point2)
    for k in np.linspace(point1, point2, 200):
        y_lam.append(k)
    for k in np.linspace(point1, point2, 200):
        x_lam1.append(k)
    point1 = point2


x_lam_ = []
y_lam_ = []
x_lam1_ = []
y_lam1_ = []
point1_ = -point
for i in range(N):
    point2_ = function_vadim(point1_, param)
    for k in range(200):
        x_lam_.append(point1_)
        y_lam1_.append(point2_)
    for k in np.linspace(point1_, point2_, 200):
        y_lam_.append(k)
    for k in np.linspace(point1_, point2_, 200):
        x_lam1_.append(k)
    point1_ = point2_


y1 = []
y2 = []


for i in x:
    y1.append(function_vadim(i, param))
    y2.append(i)


fig = plt.figure(figsize=(7, 7))
fig.patch.set_visible(False)
ax1 = fig.add_subplot(111)
ax1.set_xlabel('x')
ax1.set_ylabel('y')
plt.tight_layout()
ax1.grid(which='major', color='k')
# ax1.minorticks_on()
# ax1.grid(which='minor', color='gray', linestyle=':')


ax1.scatter(x, y1, s=0.8, c='black')
ax1.plot(x, x, linestyle='--')


ax1.scatter(x_lam[1:], y_lam[1:], s=0.5, c='violet')
ax1.scatter(x_lam1[1:], y_lam1[1:], s=0.5, c='violet')
ax1.scatter(x_lam_[1:], y_lam_[1:], s=0.1, c='red')
ax1.scatter(x_lam1_[1:], y_lam1_[1:], s=0.1, c='red')
# plt.savefig("kek.png", dpi=800)
plt.show()
