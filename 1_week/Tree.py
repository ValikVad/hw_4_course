from typing import List
import numpy as np
import matplotlib.pyplot as plt


def function_vadim(input_point: float, input_param: List[float]) -> float:
    ret_point = input_param[0] * np.sin(input_point) * (1 - np.sin(input_point))
    return ret_point


def function_tat(input_point: float, input_param: List[float]) -> float:
    ret_point = input_param[0] * input_point * (1 - input_point ** 2)
    return ret_point


point = 0.0001
N = 1200
number_of_steps = 600
start = 0
end = 6
number_of_b = 2000
b = np.linspace(start, end, number_of_b)

x = []
y = []

for i in range(number_of_b):
    print(i)

    list_of_point = list()
    list_of_point.append(point)
    params = [b[i]]
    for j in range(N):
        list_of_point.append(function_vadim(list_of_point[-1], params))
    x.append(list_of_point[:-number_of_steps:-1])
    y.append([b[i]]*(number_of_steps - 1))

plt.scatter(y, x, s=0.005, c='blue')

plt.show()